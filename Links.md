# Resources and useful links

## Recursion

[What is Recursion?](https://www.sparknotes.com/cs/recursion/whatisrecursion/section1/)

## Computing

[What is ASCII && UTF-8](https://www.youtube.com/watch?v=I-pQH_krD0M&t=5s) (VIDEO)

[Compiler and Interpreter: Compiled Language vs Interpreted Programming Languages](https://www.youtube.com/watch?v=I1f45REi3k4) (VIDEO)

## Command Line Resources

[The Art of the Command Line](https://github.com/jlevy/the-art-of-command-line)

[Write down a command-line to see the help text that matches each argument](https://explainshell.com/)

[Oh my Zsh!](https://github.com/ohmyzsh/ohmyzsh)

[VimWiki: a personal wiki for Vim](https://github.com/vimwiki/vimwiki)

## Git

[Official git site and tutorial](https://git-scm.com/)

[GitHub guides](https://guides.github.com/)

[Commands cheatsheet](https://education.github.com/git-cheat-sheet-education.pdf)

[Interactive git tutorial](https://try.github.io/)

[Visual/interactive cheatsheet](http://ndpsoftware.com/git-cheatsheet.html)

[GitKraken - A great Git GUI client for Windows, Mac & Linux](https://www.gitkraken.com/)

[Learn Git Branching](https://learngitbranching.js.org/)

[Git 101 with Exercises](https://gist.github.com/peterhurford/4d43aa5d6de114c0c741ba664c9c5ff5)
