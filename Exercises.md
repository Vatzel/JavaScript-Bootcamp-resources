# Exercises

## ICDL

<br/>

### Variables

#### 1. Swap the values

Initialize two variables A and B
Write a piece of code that will swap the values of A and B

Hint: introduce an additional variable.

<details>
<summary>Solution</summary>

	var A = 10;
	var B = 20;
	document.write("Πριν: η μεταβλητή Α είναι " + Α + "και η μεταβλητή Β είναι " + Β);
	var c = A;
	A = B;
	B = c;
	document.write('<br>');
	document.write('Πριν: η μεταβλητή Α είναι " + Α + "και η μεταβλητή Β είναι " + Β);

</details>
<br>

#### 2. Coffee teller

Why pay a coffee teller when you can just program your fortune yourself?

Store the following into variables:
*job title
*month salary
*work location
*partner's name
*number of kids

Output your fortune to the screen like so: <br />
**"You will be a X with a salary of Y, working in Z where you will meet and marry K, and make family with N kids."**

<details>
<summary>Solution</summary>

	var numKids  = 3;
	var partner  = 'Madonna';
	var geoLocation = 'New York';
	var jobTitle = 'web developer';
	var salary = 5000;

	var future = 'You will be a ' + jobTitle + ' in ' + geoLocation + ' with a salary of ' + salary +  ' dollars, and married to ' + partner + ' with ' + numKids + ' kids.';

	document.write(future);

</details>
<br/>

### True or false

1.[Remove comments to see the results](https://js.do/Corto/509722)

<br/>

### Arrays

#### EU North & South

A. [Add and remove EU countries](https://js.do/Corto/510392)

B. To Slice() or to splice()?

<details><summary>Solution</summary>

	var teams = ['AEK', 'Pao', 'PAOK', 'Aris', 'Heraklis', 'OSFP'];
	
	// Διαχωρίστε τις ομάδες της Αθήνας, του Πειραιά και της Θεσσαλονίκης 
	var athenians = teams.slice(0,2);
	var peraias = teams.slice(5);
	var thessaloniki = teams.slice(2,5);

	//Προσθέστε στις Αθηναϊκές τον Απόλλωνα 
	//Στις Θεσσαλονικιές προσθέστε την Καλαμαριά αφαιρώντας ταυτόχρονα τον Ηρακλή
	//Ενώστε Αθηναϊκές και Πειραιώτικες 
	var attica = athenians.concat(peraias);
	athenians.splice(1,0,'Apollon');
	thessaloniki.splice(2,1,'Kalamaria');

	document.write(teams, '<br>');
	document.write(thessaloniki, '<br>');
	document.write(athenians, '<br>');

</details>
<br/>

### Enhance your code

#### Find birth year

Write a code that asks from the user his year of birth.
Store this value to a variable with a descriptive name
Store to another variable with a descriptive name the current year
Subtract from the current year his year of birth and store this value to a variable with another descriptive name
Print his age to the console

<details>
<summary>Solution</summary>

	var userBirthYear = prompt('Ποιο έτος γεννήθηκες;');
	var currentYear = 2020;
	var userAge = currentYear - userBirthYear;

	document.write('Είσαι ' + userAge + ' χρονών.');

</details>
<br/>

### If - else

A. Write a function named *greaterNum* that:

* takes 2 arguments, both numbers.
* returns whichever number is the greater (higher) number.

Call that function 2 times with different number pairs, and log the output to make sure it works (e.g. "The greater number of 1 and 2 is 2.").

<details>
<summary>Solution</summary>

	function greaterNum(num1, num2) {
		if (num1 > num2) {
			return num1;
		} else {
			return num2;
		}
	}

	document.write(greaterNum(1, 2));
	document.write('<br/>', greaterNum(10, 20));
	
	//BONUS : One liner

    function greaterNum(num1, num2) {
	    return Math.max(num1, num2) 
    }

</details><br/>

B. Write a function named *helloWorld* that:

* takes 1 argument, a language code (e.g. "es", "el", "en")
    returns "Hello, World" for the given language, for at least 3 languages. It should default to returning English.

Call that function for each of the supported languages and print the result to make sure it works.
<details>
<summary>Solution</summary>

	function helloWorld(lang) {
		if (lang == 'el') {
			return 'Γειά σου κόσμε!';
		} else if (lang == 'es') {
			return 'Hola, Mundo!';
		} else if (lang == 'fr') {
			return 'Bonjour tout le monde!';
		} else {
			return 'Hello, World!';
		}
	}

	document.write(helloWorld('el'));
	document.write('<br/>', helloWorld('fr'));
	document.write('<br/>', helloWorld('es'));

</details>
<br/>

### Loops

A. Write a for loop that will iterate from 0 to 20. For each iteration, it will check if the current number is even or odd, and report that to the screen (e.g. "2 is even").

<details>
<summary>Solution</summary>

	for (var i = 0; i <= 20; i++) {
		if (i % 2 === 0) {
			document.write('<br/>', i + ' is even');
			} else {
				document.write('<br/>', i + ' is odd');
			}
	}

</details><br/>

B. Write a for loop that will iterate from 0 to 10. For each iteration of the for loop, it will multiply the number by 2 and write the result (e.g. "2 * 1 = 2").

<details>
<summary>Solution</summary>

	var multiplier = 2;
	for (var i = 0; i <= 10; i++) {
		var result = multiplier * i;
		document.write('<br/>', multiplier + ' * ' + i + ' = ' + result);
	}

</details><br/>

### Objects

#### String()

Elytis poem
<details>
<summary>Solution</summary>

	var elytis = 'Οδυσσέας Ελύτης, 1911-1996';
	var elytisPoem = 'Μικρή Πράσινη Θάλασσα'

	//Δημιουργήστε μια νέα μεταβλητή με την ημερομηνία γέννησης του Ελύτη χρησιμοποιώντας την αρχική μεταβλητή
	
	var elytisLife = elytis.substr(17,9);
	document.write(elytisLife,'<br>');

	//Φτιάξτε μια function που να τυπώνει την πρόταση 'πράσινη θάλασσα' 
	
	function prasini() { 
		var green_sea = elytisPoem.substr(6); 
		document.write(green_sea); 
	} 
	prasini();

	document.write('<br>');

	//Μετατρέψτε τον τίτλο του ποιήματος σε πεζά
	
	var littleGreenSea = elytisPoem.toLowerCase();
	document.write(littleGreenSea, '<br>');

	//Υπάρχει κάποιο γράμμα π και σε ποια θέση βρίσκεται; Φτιάξτε μια function για αυτό. 
	
	function π() { 
		var pIndex = littleGreenSea.indexOf('π'); 
		document.write(pIndex);  
	} 

	π(); 
	document.write('<br>');
</details><br>

### Events

[Events examples](https://js.do/Corto/doubleclickevent)
<br>

### Review exercises

A. Life's coffee supply
<details>
<summary>Solution</summary>

	function calculateSupply(age, numPerDay) {
		var maxAge = 90;
		var totalNeeded = (numPerDay * 365) * (maxAge - age);
		var message = 'You will need ' + totalNeeded + ' cups of coffee to last you until the ripe old age of ' + maxAge;
		return message;
		}

    document.write(calculateSupply(28, 36),'<br/>');
    document.write(calculateSupply(28, 2.5),'<br/>');
    document.write(calculateSupply(28, 400),'<br/>');

</details><br>

B. Print a star pattern
<details>
<summary>Solution</summary>

	for(var i = 1; i <= 20; i++) {
      for(var j = 1; j <= i; j++) {
        document.write('*');
      }
      document.write('<br/>');
    }
</details><br>

C. Calculate BMI
<details>
<summary>Exercise</summary>
Write a simple program where:
Height is a variable named ht
Weight is a variable wg

BMI is calculated based on the formula:

* BMI = wg / (ht * ht)

The BMI is then evaluated as:

* Underweight = <18.5
* Normal weight = 18.5–24.9
* Overweight = 25–29.9
* Obesity = BMI of 30 or greater

</details>

[Solution](https://js.do/Corto/bmi)
