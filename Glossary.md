**Element**

*“Element”* is the official name for a node of a HTML document.
For example, `<p>There is something here</p>` is a string representation of a HTML element.

**Tag**

*“Tag”* is part of the syntax to represent a HTML node.
For example, in `<p>…</p>`. The `<p>` is “opening tag”, and `</p>` is “closing tag”.
Remember, some element do not have a closing tag.

**Node**

*“Node”* is a node of tree of Document Object Model (aka DOM).
A HTML element is a node. A XML element is a node.
The abcd in `<p>abcd</p>` is a node. It is a child node of the element “p”
Whitespace between elements is also a node.

**Object**

In JavaScript, “object” is one of its datatype. It is characterized as a collection of key/value pairs.

In HTML/XML, when a element is parsed, it becomes a node in the DOM tree. This node, is also called object, when we want to talk about programing it.

In other words, HTML tag represents element, and it becomes node in DOM, and becomes object in JavaScript.

**Property**

In CSS, {color, width, border, font-family, …} are properties.
In JavaScript, properties are the key/value pairs attached to object.
In DOM, “property” is a “instance variable”, it corresponds to HTML's “attribute”.
For example, HTML elements has {id, class}, attributes. In DOM, they are called “properties”.

**Attribute**

In HTML, “attribute” are things like {id, class, href, src, …} of a HTML element.

In JavaScript, “attribute” is a “special” info attached to property. That is, in JavaScript, each object datatype has properties (which are key/value pairs), and each “property” has attributes such as {writable, enumerable, configurable}.

**Method**

In JavaScript, a “method”, is a property whose value is a function.
So, a method is also a property, technically.

In DOM, the term “property” seems to follow JavaScript, but, properties that are not functions are referred to as “property” proper. For example, { .length, .name, .nodeName, .parent .onclick, .baseURL, .characterSet, .color, …} are all properties, but “getElementById” is a “method”.

Sources:

[JavaScript DOM confusing terminology](http://xahlee.info/js/javascript_DOM_confusing_terminology.html)
