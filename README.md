# JavaScript Bootcamp Resources

Σημειώσεις, πηγές για μελέτη, ασκήσεις και Q&A για το PeopleCert JavaScript Bootcamp.

* [Ασκήσεις](Exercises.md)
* [Q&A](Q&A.md)
* [Links & additional resources](Links.md)
