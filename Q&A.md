# Q&A

## JavaScript <hr>

**Can JavaScript calculate very big numbers? How will present it?**

For very big or very small numbers, you can use scientific notation by adding an e (for exponent), followed by the exponent of the number.

    let bigNumber = 534e8;       // 53400000000
    let smallNumber = 534e-8;      // 0.00000534

**Is semicolon (;) necessary in JavaScript?**

JavaScript semicolons are optional. This is all possible because JavaScript does not strictly require semicolons. When there is a place where a semicolon was needed, it adds it behind the scenes. The process that does this is called Automatic Semicolon Insertion (ASI).
Omitting semicolons in JS is big debate, but we should always keep semicolon.

**How sort() is sorting numbers?**

Sort() method sorts the values as *strings* in alphabetical and ascending order.
This mechanism works well for strings ("Anna" comes before "Chris"). However, if numbers are sorted as strings, "25" is bigger than "100", because "2" is bigger than "1".
To fix this and in order to sort an array in ascending order you need to pass a comparing function to sort:

    array.sort(function(a,b){return a-b});

**Can you give me a visualization of try-catch-finally?**

You can find a visualization [here](http://www.pythontutor.com/javascript.html#code=%0Atry%20%7B%0A%20%20console.log%28'Start%20of%20try%20runs'%29%3B%0A%20%20%0A%20%20unicycle%3B%0A%0A%20%20console.log%28'End%20of%20try%20runs%20--%20never%20reached'%29%3B%20%0A%0A%7D%20catch%28err%29%20%7B%0A%0A%20%20console.log%28'Error%20has%20occured%3A%20'%20%2B%20err%29%3B%20%0A%0A%7D%20finally%20%7B%0A%20%20console.log%28'This%20is%20always%20run'%29%3B%20%0A%7D%0A%0Aconsole.log%28'...Then%20the%20execution%20continues'%29%3B%0A%0A&curInstr=0&mode=display&origin=opt-frontend.js&py=js&rawInputLstJSON=%5B%5D)

## Front End <hr>

**[What are the difference between static and dynamic pages?](https://www.geeksforgeeks.org/difference-between-static-and-dynamic-web-pages/)**

## Software Development <hr>

**Can you give real examples about SaaS, PaaS & IaaS?**

*SaaS examples*:<br> BigCommerce, Google Apps, Salesforce, Dropbox, MailChimp, ZenDesk, DocuSign, Slack, Hubspot, Cisco WebEx.

*PaaS examples:* <br>AWS Elastic Beanstalk, Heroku, Windows Azure (mostly used as PaaS), Force com, OpenShift, Magento Commerce Cloud, IBM Cloud.

*IaaS examples:* <br>Amazon Web Services (AWS EC2), Rackspace, Google Compute Engine (GCE), Digital Ocean, Linode.
